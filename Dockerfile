# build stage
FROM microsoft/aspnetcore-build:latest AS build-env

WORKDIR /generator

# restore
# split up to make the API a seperate layer from the test, so if test changes we do not need to rebuild API layer, we can reuse the API layer.
COPY api/api.csproj ./api/
RUN dotnet restore api/api.csproj

COPY tests/tests.csproj ./tests/
RUN dotnet restore tests/tests.csproj

# copy src
COPY . .

# test
ENV TEAMCITY_PROJECT_NAME = ${TEAMCITY_PROJECT_NAME}
RUN dotnet test --verbosity=normal tests/tests.csproj

# publish
RUN dotnet publish api/api.csproj -o /publish

# runtime stage
FROM microsoft/aspnetcore:latest
COPY --from=build-env /publish /publish
WORKDIR /publish
ENTRYPOINT [ "dotnet", "api.dll" ]