using System;
using Xunit;
using api.Controllers;
using System.Linq;

namespace tests
{
    public class RangeTests
    {
        [Fact]
        public void CountShouldControlNumberOfResults()
        {
            //Given
            var range = new Range { Count = 3 };

            //When
            var generated = range.Of(() => "");

            //Then
            Assert.Equal(range.Count, generated.Count());
        }

        [Fact]
        public void SortShouldOrderResults()
        {
            //Given
            var range = new Range { Count = 3, Sort = true };
            var values = new[] { "c", "a", "b" };
            var counter = 0;

            //When
            var generated = range.Of(() => values[counter++]);

            //Then
            Assert.Equal(new[] { "a", "b", "c" }, generated.ToArray());
        }
    }
}
